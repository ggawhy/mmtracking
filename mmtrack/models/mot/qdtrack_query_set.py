# Copyright (c) OpenMMLab. All rights reserved.
# from msilib import sequence
import torch
from mmdet.models import build_detector, build_head

from mmtrack.core import outs2results, results2outs
from mmtrack.models.mot import BaseMultiObjectTracker
from ..builder import MODELS, build_tracker
from mmtrack.models.mot import QDTrack
import mmcv

@MODELS.register_module()
class QDTrackQuerySet(QDTrack):
    """Quasi-Dense Similarity Learning for Multiple Object Tracking.

    This multi object tracker is the implementation of `QDTrack
    <https://arxiv.org/abs/2006.06664>`_.

    Args:
        detector (dict): Configuration of detector. Defaults to None.
        track_head (dict): Configuration of track head. Defaults to None.
        tracker (dict): Configuration of tracker. Defaults to None.
        freeze_detector (bool): If True, freeze the detector weights.
            Defaults to False.
    """
    def __init__(self, prior_path=None, max_query_num=None, *args, **kwargs):
        if prior_path!=None:
            self.with_prior_query = True
            self.prior_query_index = mmcv.load(prior_path)
        else:
            self.with_prior_query = False

        self.max_query_num = max_query_num


        super().__init__(*args, **kwargs)

    def simple_test(self, img, img_metas, gt_bboxes, rescale=False):
        """Test forward.

         Args:
            img (Tensor): of shape (N, C, H, W) encoding input images.
                Typically these should be mean centered and std scaled.
            img_metas (list[dict]): list of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
            rescale (bool): whether to rescale the bboxes.

        Returns:
            dict[str : Tensor]: Track results.
        """
        # TODO inherit from a base tracker
        assert self.with_track_head, 'track head must be implemented.'  # noqa
        frame_id = img_metas[0].get('frame_id', -1)
        if frame_id == 0:
            self.tracker.reset()

        x = self.detector.extract_feat(img)
        #gy_add
        if frame_id == 0:
            #gy_add
            self.querybboxes = []
            self.detector.query_feats = []
            if self.with_prior_query:
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                seq_name = img_metas[0]['filename'].split('/')[-3]
                is_random = False
                if is_random:
                    import random
                    good_nums = len(self.prior_query_index[seq_name])
                    rand_idx = random.randint(0, good_nums-1)
                    query_index = self.prior_query_index[seq_name][rand_idx]
                else:
                    # query_index = self.prior_query_index[seq_name][0]
                    # query_index = self.prior_query_index[seq_name][1]
                    query_index = self.prior_query_index[seq_name][2]
                
                query_index = torch.tensor(query_index).to(img).long()
                # self.query_bbox = [gt_bboxes[query_index].unsqueeze(0)] 
                # self.detector.query_feat = self.detector.extract_query_feat(x, self.query_bbox)                
                self.querybboxes = gt_bboxes[query_index].unsqueeze(0)
            else:
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                random_bbox_inx = torch.randint(gt_bboxes.shape[0],(1,))
                # self.query_bbox = [gt_bboxes[random_bbox_inx]]  #[torch.Size([1, 4])]
                self.querybboxes = gt_bboxes[random_bbox_inx]  #[torch.Size([1, 4])]
                # self.detector.query_feat = self.detector.extract_query_feat(x, self.query_bbox)
                # gy_add
            self.detector.init_query_feat = self.detector.extract_query_feat(x, [self.querybboxes])
            # self.detector.query_feats = self.detector.extract_query_feat(x, [self.querybboxes])
            self.detector.query_feats = self.detector.extract_query_feat(x, [self.querybboxes])
                     
        proposal_list = self.detector.rpn_head.simple_test_rpn(x, img_metas)
        all_det_bboxes = []
        all_det_labels = []
        det_idx = []
        query_nums = self.detector.query_feats.shape[0]
        for i in range(query_nums):
            det_results  = self.detector.roi_head.simple_test_bboxes(# [torch.Size([100, 5]), torch.Size([100])]
                    x,
                    img_metas,
                    self.detector.query_feats[i:i+1,:,:,:],
                    proposal_list,
                    self.detector.roi_head.test_cfg,
                    rescale=rescale)
            all_det_bboxes.append(det_results[0][0])
            all_det_labels.append(det_results[1][0])
            det_idx.append(det_results[1][0].size(0))
        for i in range(self.detector.query_feats.shape[0]):
            temp_bboxes = all_det_bboxes[0] if i==0 else torch.cat((temp_bboxes, all_det_bboxes[i]), dim=0)
            temp_labels = all_det_labels[0] if i==0 else torch.cat((temp_labels, all_det_labels[i]))

        # if query_nums==1:
        #     all_det_bboxes = all_det_bboxes[0]
        #     all_det_labels = all_det_labels[0]
        # else:
        #     for i in range(self.detector.query_feats.shape[0]):
        #         temp_bboxes = torch.cat((all_det_bboxes), dim=0)
        #         temp_labels = torch.cat((), dim=0)
        #     all_det_bboxes = temp_bboxes
        #     all_det_labels = temp_labels
        
        #nms
        from mmdet.core import multiclass_nms
        nms = {'method': 'linear', 'type': 'soft_nms', 'iou_thr': 0.5, 'min_score': 0.001}
        bboxes = temp_bboxes[:,:4]
        scores = torch.cat((temp_bboxes[:,4].unsqueeze(1), (1-temp_bboxes[:,4]).unsqueeze(1)),dim=1)
        det_bboxes, det_labels = multiclass_nms(bboxes, scores, 0.05, nms, min(50,bboxes.size(0)))#bboxes:torch.Size([1000, 4]), scores:torch.Size([1000, 2])
                                                    
        # det_bboxes = temp_bboxes
        # det_labels = temp_labels
        sorted, indices = torch.sort(det_bboxes[:,4], descending=True)
        det_bboxes = det_bboxes[indices]

        # add_way = 'hard add'
        # if add_way == 'hard add':
        #     MAX_QUERY_NUM = 2
        #     hard_thr = 0.5
        #     vaildnum = torch.sum(det_bboxes[:,4] > hard_thr)
        #     if vaildnum != 0:
        #         for i in range(min(vaildnum, MAX_QUERY_NUM-1)):
        #             if len(self.querybboxes) < MAX_QUERY_NUM:
        #                 self.querybboxes = torch.cat((self.querybboxes, det_bboxes[i,:4].unsqueeze(0) \
        #                     * torch.tensor(img_metas[0]['scale_factor']).to(img.device)))
        #             else:
        #                 self.querybboxes[i+1,:] = det_bboxes[i,:4] * torch.tensor(img_metas[0]['scale_factor']).to(img.device)
        #     self.detector.query_feats = self.detector.extract_query_feat(x, [self.querybboxes]) #!!!这个地方逻辑有错，不应该重提特征，x已经不是第一帧的图片了，但是框还是第一帧的框
        #     self.detector.query_feats[0,:]  = self.detector.init_query_feat
        add_way = 'hard add'
        if self.max_query_num!=None:
            MAX_QUERY_NUM = self.max_query_num
        else:
            MAX_QUERY_NUM = 2

        if add_way == 'hard add':            
            if(sum(det_idx)!=0):
                for i in range(min(det_bboxes.shape[0], MAX_QUERY_NUM-1)):
                    box = det_bboxes[i,:4].unsqueeze(0) * torch.tensor(img_metas[0]['scale_factor']).to(img.device)
                    self.querybboxes = box if i==0 else torch.cat((self.querybboxes, box), dim=0)
                self.detector.query_feats = self.detector.extract_query_feat(x, [self.querybboxes])
                self.detector.query_feats  = torch.cat((self.detector.init_query_feat, self.detector.query_feats),dim=0)
        elif add_way == 'soft add':
            thr = 0.5
            vaildnum = torch.sum(det_bboxes[:,4] > thr)
            if sum(det_idx)!=0 and vaildnum != 0:
                for i in range(min(vaildnum, MAX_QUERY_NUM-1)):
                    box = det_bboxes[i,:4].unsqueeze(0) * torch.tensor(img_metas[0]['scale_factor']).to(img.device)
                    self.querybboxes = box if i==0 else torch.cat((self.querybboxes, box), dim=0)

        
        
        num_classes = 1
        # track_bboxes：torch.Size([214, 5])，track_labels：torch.Size([214])，track_ids：torch.Size([214])
        track_bboxes, track_labels, track_ids = self.tracker.track(
            img_metas=img_metas,
            feats=x,
            model=self,
            bboxes=det_bboxes,
            labels=det_labels,
            frame_id=frame_id)

        track_bboxes = outs2results(#长度482的list，每个元素（y,6）
            bboxes=track_bboxes,
            labels=track_labels,
            ids=track_ids,
            num_classes=num_classes)['bbox_results']
        
        # gy_add
        bbox_results = outs2results(
            bboxes=det_bboxes, labels=det_labels, num_classes=num_classes)

        return dict(det_bboxes=bbox_results, track_bboxes=track_bboxes)
