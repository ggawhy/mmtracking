# Copyright (c) OpenMMLab. All rights reserved.
# from msilib import sequence
import torch
from mmdet.models import build_detector, build_head

from mmtrack.core import outs2results, results2outs
from mmtrack.models.mot import BaseMultiObjectTracker
from ..builder import MODELS, build_tracker
from mmtrack.models.mot import QDTrack
import mmcv

@MODELS.register_module()
class QDTrackMultiQuery(QDTrack):
    """Quasi-Dense Similarity Learning for Multiple Object Tracking.

    This multi object tracker is the implementation of `QDTrack
    <https://arxiv.org/abs/2006.06664>`_.

    Args:
        detector (dict): Configuration of detector. Defaults to None.
        track_head (dict): Configuration of track head. Defaults to None.
        tracker (dict): Configuration of tracker. Defaults to None.
        freeze_detector (bool): If True, freeze the detector weights.
            Defaults to False.
    """
    def __init__(self, prior_path=None, *args, **kwargs):
        if prior_path!=None:
            self.with_prior_query = True
            self.prior_query_index = mmcv.load(prior_path)
        else:
            self.with_prior_query = False

        super().__init__(*args, **kwargs)

    def simple_test(self, img, img_metas, gt_bboxes, gt_seq_ids, rescale=False):
        """Test forward.

         Args:
            img (Tensor): of shape (N, C, H, W) encoding input images.
                Typically these should be mean centered and std scaled.
            img_metas (list[dict]): list of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
            rescale (bool): whether to rescale the bboxes.

        Returns:
            dict[str : Tensor]: Track results.
        """
        # TODO inherit from a base tracker
        assert self.with_track_head, 'track head must be implemented.'  # noqa
        frame_id = img_metas[0].get('frame_id', -1)
        if frame_id == 0:
            self.tracker.reset()

        x = self.detector.extract_feat(img)
        #gy_add
        if frame_id == 0:
            if self.with_prior_query:
                # gt_bboxes =  gt_bboxes[0].squeeze(0)
                # seq_name = img_metas[0]['filename'].split('/')[-3]
                # is_random = False
                # if is_random:
                #     import random
                #     good_nums = len(self.prior_query_index[seq_name])
                #     rand_idx = random.randint(0, good_nums-1)
                #     query_index = self.prior_query_index[seq_name][rand_idx]
                # else:
                #     # query_index = self.prior_query_index[seq_name][0]
                #     # query_index = self.prior_query_index[seq_name][1]
                #     query_index = self.prior_query_index[seq_name][2]
                
                # query_index = torch.tensor(query_index).to(img).long()
                # self.query_bbox = [gt_bboxes[query_index].unsqueeze(0)] 
                # self.detector.query_feat = self.detector.extract_query_feat(x, self.query_bbox)
                pass
            else:
                # gt_bboxes =  gt_bboxes[0].squeeze(0)
                # random_bbox_inx = torch.randint(gt_bboxes.shape[0],(1,))
                # self.query_bbox = [gt_bboxes[random_bbox_inx]]  #[torch.Size([1, 4])]
                # self.detector.query_feat = self.detector.extract_query_feat(x, self.query_bbox)
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                gt_seq_ids = gt_seq_ids[0].squeeze(0)
                gt_nums = torch.tensor(0).to(img.device)
                self.querys = []
                self.detector.query_feats = []
                for i in range(4):
                    assert torch.sum(gt_seq_ids==i) != torch.tensor(0).to(img.device)
                    gt_nums = gt_nums + torch.sum(gt_seq_ids==i)
                    random_bbox_inx = torch.randint(gt_nums,(1,))
                    self.querys.append(gt_bboxes[random_bbox_inx])
                for i in range(4):
                    self.detector.query_feats.append(self.detector.extract_query_feat(x, [self.querys[i]]))              
                    
        proposal_list = self.detector.rpn_head.simple_test_rpn(x, img_metas)
        all_det_bboxes = []
        all_det_labels = []
        for i in range(4):
            det_results  = self.detector.roi_head.simple_test_bboxes(# [torch.Size([100, 5]), torch.Size([100])]
                    x,
                    img_metas,
                    self.detector.query_feats[i],
                    proposal_list,
                    self.detector.roi_head.test_cfg,
                    rescale=rescale)

            det_bboxes = det_results[0][0]
            det_labels = det_results[1][0] + torch.tensor(i).to(img.device)
            all_det_bboxes.append(det_bboxes)
            all_det_labels.append(det_labels)
        all_det_bboxes = torch.cat((all_det_bboxes[0],
                                    all_det_bboxes[1],
                                    all_det_bboxes[2],
                                    all_det_bboxes[3]), dim=0)
        all_det_labels = torch.cat((all_det_labels[0],
                                    all_det_labels[1],
                                    all_det_labels[2],
                                    all_det_labels[3]))
        num_classes = 4 
        # track_bboxes：torch.Size([214, 5])，track_labels：torch.Size([214])，track_ids：torch.Size([214])
        track_bboxes, track_labels, track_ids = self.tracker.track(
            img_metas=img_metas,
            feats=x,
            model=self,
            # bboxes=det_bboxes,
            # labels=det_labels,
            bboxes=all_det_bboxes,
            labels=all_det_labels,
            frame_id=frame_id)

        track_bboxes = outs2results(#长度482的list，每个元素（y,6）
            bboxes=track_bboxes,
            labels=track_labels,
            ids=track_ids,
            num_classes=num_classes)['bbox_results']
        
        # gy_add
        bbox_results = outs2results(
            bboxes=det_bboxes, labels=det_labels, num_classes=num_classes)

        return dict(det_bboxes=bbox_results, track_bboxes=track_bboxes)
