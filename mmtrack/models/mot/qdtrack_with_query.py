# Copyright (c) OpenMMLab. All rights reserved.
# from msilib import sequence
import torch
from mmdet.models import build_detector, build_head

from mmtrack.core import outs2results, results2outs
from mmtrack.models.mot import BaseMultiObjectTracker
from ..builder import MODELS, build_tracker
from mmtrack.models.mot import QDTrack
import mmcv

@MODELS.register_module()
class QDTrack_Query(QDTrack):
    """Quasi-Dense Similarity Learning for Multiple Object Tracking.

    This multi object tracker is the implementation of `QDTrack
    <https://arxiv.org/abs/2006.06664>`_.

    Args:
        detector (dict): Configuration of detector. Defaults to None.
        track_head (dict): Configuration of track head. Defaults to None.
        tracker (dict): Configuration of tracker. Defaults to None.
        freeze_detector (bool): If True, freeze the detector weights.
            Defaults to False.
    """
    def __init__(self, prior_path=None, *args, **kwargs):
        if prior_path!=None:
            self.with_prior_query = True
            self.prior_query_index = mmcv.load(prior_path)
        else:
            self.with_prior_query = False

        super().__init__(*args, **kwargs)

    def simple_test(self, img, img_metas, gt_bboxes, rescale=False):
        """Test forward.

         Args:
            img (Tensor): of shape (N, C, H, W) encoding input images.
                Typically these should be mean centered and std scaled.
            img_metas (list[dict]): list of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
            rescale (bool): whether to rescale the bboxes.

        Returns:
            dict[str : Tensor]: Track results.
        """
        # TODO inherit from a base tracker
        assert self.with_track_head, 'track head must be implemented.'  # noqa
        frame_id = img_metas[0].get('frame_id', -1)
        if frame_id == 0:
            self.tracker.reset()

        x = self.detector.extract_feat(img)
        #gy_add
        if frame_id == 0:
            if self.with_prior_query:
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                seq_name = img_metas[0]['filename'].split('/')[-3]
                is_random = False
                if is_random:
                    import random
                    good_nums = len(self.prior_query_index[seq_name])
                    rand_idx = random.randint(0, good_nums-1)
                    query_index = self.prior_query_index[seq_name][rand_idx]
                else:
                    # query_index = self.prior_query_index[seq_name][0]
                    # query_index = self.prior_query_index[seq_name][1]
                    query_index = self.prior_query_index[seq_name][2]
                
                query_index = torch.tensor(query_index).to(img).long()
                self.query_bbox = [gt_bboxes[query_index].unsqueeze(0)] 
                self.detector.query_feat = self.detector.extract_query_feat(x, self.query_bbox)
            else:
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                random_bbox_inx = torch.randint(gt_bboxes.shape[0],(1,))
                # with open('/home/gaoyun/Desktop/mmtracking/workship/qg_qdtrack/query.txt', 'a') as f:
                #     if frame_id!=35:
                #         f.write(str(random_bbox_inx.item()) + '\n')
                #     else:
                #         f.write(str(random_bbox_inx.item()) + ' ')
                self.query_bbox = [gt_bboxes[random_bbox_inx]]  #[torch.Size([1, 4])]
                self.detector.query_feat = self.detector.extract_query_feat(x, self.query_bbox)

                #gy_add for vis
                # import cv2
                # import os
                # save_dir = '/home/gaoyun/Desktop/mmtracking/GMOT_results/DanceTrack_frame1_vis/'
                # gt_bboxes = gt_bboxes / torch.tensor(
                #                     img_metas[0]['scale_factor']).to(img.device)
                # gt_bboxes = gt_bboxes.cpu().numpy().astype(int)
                # full_file_name = img_metas[0]['filename']
                # img = cv2.imread(full_file_name)
                # for i in range(gt_bboxes.shape[0]):
                #     cv2.rectangle(img, gt_bboxes[i, :2], gt_bboxes[i, 2:4], (0,255,0), 2)
                #     cv2.putText(img, str(i), gt_bboxes[i, 2:4], cv2.FONT_HERSHEY_SIMPLEX, 0.75, color=(0, 0, 255), thickness=1)
                # file_name = full_file_name.split('/')[-1]
                # file_name = 'model' + file_name
                # class_name = full_file_name.split('/')[-3]
                # if not os.path.exists(os.path.join(save_dir, class_name)):
                #     os.mkdir(os.path.join(save_dir, class_name))
                # save_path = os.path.join(os.path.join(save_dir, class_name), file_name)
                # cv2.imwrite(save_path, img)
                
            
        
        proposal_list = self.detector.rpn_head.simple_test_rpn(x, img_metas)

        det_results  = self.detector.roi_head.simple_test_bboxes(# [torch.Size([100, 5]), torch.Size([100])]
                x,
                img_metas,
                self.detector.query_feat,
                proposal_list,
                self.detector.roi_head.test_cfg,
                rescale=rescale)

        det_bboxes = det_results[0][0]
        det_labels = det_results[1][0]
        num_classes = 1
        # track_bboxes：torch.Size([214, 5])，track_labels：torch.Size([214])，track_ids：torch.Size([214])
        track_bboxes, track_labels, track_ids = self.tracker.track(
            img_metas=img_metas,
            feats=x,
            model=self,
            bboxes=det_bboxes,
            labels=det_labels,
            frame_id=frame_id)

        track_bboxes = outs2results(#长度482的list，每个元素（y,6）
            bboxes=track_bboxes,
            labels=track_labels,
            ids=track_ids,
            num_classes=num_classes)['bbox_results']
        
        # gy_add
        bbox_results = outs2results(
            bboxes=det_bboxes, labels=det_labels, num_classes=num_classes)

        return dict(det_bboxes=bbox_results, track_bboxes=track_bboxes)
