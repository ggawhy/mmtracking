# Copyright (c) OpenMMLab. All rights reserved.
import warnings
import torch

from mmdet.models import build_detector

from mmtrack.core import outs2results
from ..builder import MODELS, build_motion, build_reid, build_tracker
from .base import BaseMultiObjectTracker
from .deep_sort import DeepSORT
import mmcv

@MODELS.register_module()
class DeepSORT_Query(DeepSORT):
    """Simple online and realtime tracking with a deep association metric.

    Details can be found at `DeepSORT<https://arxiv.org/abs/1703.07402>`_.
    """

    def __init__(self, prior_path=None, *args, **kwargs):
        if prior_path!=None:
            self.with_prior_query = True
            self.prior_query_index = mmcv.load(prior_path)
        else:
            self.with_prior_query = False

        super().__init__(*args, **kwargs)

    def forward_train(self, *args, **kwargs):
        """Forward function during training."""
        raise NotImplementedError(
            'Please train `detector` and `reid` models firstly, then \
                inference with SORT/DeepSORT.')

    def simple_test(self,
                    img,
                    img_metas,                    
                    gt_bboxes,
                    rescale=False,
                    public_bboxes=None,
                    **kwargs):
        """Test without augmentations.

        Args:
            img (Tensor): of shape (N, C, H, W) encoding input images.
                Typically these should be mean centered and std scaled.
            img_metas (list[dict]): list of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
            rescale (bool, optional): If False, then returned bboxes and masks
                will fit the scale of img, otherwise, returned bboxes and masks
                will fit the scale of original image shape. Defaults to False.
            public_bboxes (list[Tensor], optional): Public bounding boxes from
                the benchmark. Defaults to None.

        Returns:
            dict[str : list(ndarray)]: The tracking results.
        """
        frame_id = img_metas[0].get('frame_id', -1)
        if frame_id == 0:
            self.tracker.reset()
            
        x = self.detector.extract_feat(img)
        #gy_add
        if frame_id == 0:
            if self.with_prior_query:
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                seq_name = img_metas[0]['filename'].split('/')[-3]
                is_random = False
                if is_random:
                    import random
                    good_nums = len(self.prior_query_index[seq_name])
                    rand_idx = random.randint(0, good_nums-1)
                    query_index = self.prior_query_index[seq_name][rand_idx]
                else:
                    query_index = self.prior_query_index[seq_name][2]
                
                query_index = torch.tensor(query_index).to(img).long()
                self.query_bbox = [gt_bboxes[query_index].unsqueeze(0)] 
                self.detector.query_feat = self.detector.extract_query_feat(x, self.query_bbox)

            else:
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                random_bbox_inx = torch.randint(gt_bboxes.shape[0],(1,))
                bbox = [gt_bboxes[random_bbox_inx]]      

                self.detector.query_feat = self.detector.extract_query_feat(x, bbox)
        
        if hasattr(self.detector, 'roi_head'):
            # TODO: check whether this is the case
            if public_bboxes is not None:
                public_bboxes = [_[0] for _ in public_bboxes]
                proposals = public_bboxes
            else:
                proposals = self.detector.rpn_head.simple_test_rpn(
                    x, img_metas)
        
            # TODO: support batch inference
            det_bboxes = det_bboxes[0]
            det_labels = det_labels[0]
            num_classes = self.detector.roi_head.bbox_head.num_classes            

        elif hasattr(self.detector, 'bbox_head'):
            outs = self.detector.bbox_head(x)
            result_list = self.detector.bbox_head.get_bboxes(
                *outs, img_metas=img_metas, rescale=rescale)
            # TODO: support batch inference
            det_bboxes = result_list[0][0]
            det_labels = result_list[0][1]
            num_classes = self.detector.bbox_head.num_classes
        else:
            raise TypeError('detector must has roi_head or bbox_head.')

        track_bboxes, track_labels, track_ids = self.tracker.track(
            img=img,
            img_metas=img_metas,
            model=self,
            feats=x,
            bboxes=det_bboxes,
            labels=det_labels,
            frame_id=frame_id,
            rescale=rescale,
            **kwargs)
        # gy_add
        vis_track_feature = self.tracker.tracks

        track_results = outs2results(
            bboxes=track_bboxes,
            labels=track_labels,
            ids=track_ids,
            num_classes=num_classes)
        det_results = outs2results(
            bboxes=det_bboxes, labels=det_labels, num_classes=num_classes)

        return dict(
            det_bboxes=det_results['bbox_results'],
            track_bboxes=track_results['bbox_results'])
