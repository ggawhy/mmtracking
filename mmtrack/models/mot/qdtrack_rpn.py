# Copyright (c) OpenMMLab. All rights reserved.
import torch
from mmdet.models import build_detector, build_head

from mmtrack.core import outs2results, results2outs
from mmtrack.models.mot import BaseMultiObjectTracker
from ..builder import MODELS, build_tracker


@MODELS.register_module()
class QDTrack_RPN(BaseMultiObjectTracker):
    def __init__(self,
                 detector=None,
                 track_head=None,
                 tracker=None,
                 freeze_detector=False,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        if detector is not None:
            self.detector = build_detector(detector)

        if track_head is not None:
            self.track_head = build_head(track_head)

        if tracker is not None:
            self.tracker = build_tracker(tracker)

        self.freeze_detector = freeze_detector
        if self.freeze_detector:
            self.freeze_module('detector')

    def forward_train(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      gt_match_indices,
                      ref_img,
                      ref_img_metas,
                      ref_gt_bboxes,
                      ref_gt_labels,
                      gt_bboxes_ignore=None,
                      gt_masks=None,
                      ref_gt_bboxes_ignore=None,
                      ref_gt_masks=None,
                      **kwargs):
        """Forward function during training.

         Args:
            img (Tensor): of shape (N, C, H, W) encoding input images.
                Typically these should be mean centered and std scaled.
            img_metas (list[dict]): list of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
            gt_bboxes (list[Tensor]): Ground truth bboxes of the image,
                each item has a shape (num_gts, 4).
            gt_labels (list[Tensor]): Ground truth labels of all images.
                each has a shape (num_gts,).
            gt_match_indices (list(Tensor)): Mapping from gt_instance_ids to
                ref_gt_instance_ids of the same tracklet in a pair of images.
            ref_img (Tensor): of shape (N, C, H, W) encoding input reference
                images. Typically these should be mean centered and std scaled.
            ref_img_metas (list[dict]): list of reference image info dict where
                each dict has: 'img_shape', 'scale_factor', 'flip', and may
                also contain 'filename', 'ori_shape', 'pad_shape',
                and 'img_norm_cfg'.
            ref_gt_bboxes (list[Tensor]): Ground truth bboxes of the
                reference image, each item has a shape (num_gts, 4).
            ref_gt_labels (list[Tensor]): Ground truth labels of all
                reference images, each has a shape (num_gts,).
            gt_masks (list[Tensor]) : Masks for each bbox, has a shape
                (num_gts, h , w).
            gt_bboxes_ignore (list[Tensor], None): Ground truth bboxes to be
                ignored, each item has a shape (num_ignored_gts, 4).
            ref_gt_bboxes_ignore (list[Tensor], None): Ground truth bboxes
                of reference images to be ignored,
                each item has a shape (num_ignored_gts, 4).
            ref_gt_masks (list[Tensor]) : Masks for each reference bbox,
                has a shape (num_gts, h , w).

        Returns:
            dict[str : Tensor]: All losses.
        """
        x = self.detector.extract_feat(img)

        losses = dict()

        # RPN forward and loss
        if self.detector.with_rpn:
            proposal_cfg = self.detector.train_cfg.get(
                'rpn_proposal', self.detector.test_cfg.rpn)
            rpn_losses, proposal_list = self.detector.rpn_head.forward_train(
                x,
                img_metas,
                gt_bboxes,
                gt_labels=None,
                gt_bboxes_ignore=gt_bboxes_ignore,
                proposal_cfg=proposal_cfg)
            losses.update(rpn_losses)

        ref_x = self.detector.extract_feat(ref_img)
        ref_proposals = self.detector.rpn_head.simple_test_rpn(
            ref_x, ref_img_metas)

        track_losses = self.track_head.forward_train(
            x, img_metas, proposal_list, gt_bboxes, gt_labels,
            gt_match_indices, ref_x, ref_img_metas, ref_proposals,
            ref_gt_bboxes, ref_gt_labels, gt_bboxes_ignore, gt_masks,
            ref_gt_bboxes_ignore)

        losses.update(track_losses)

        return losses

    def simple_test(self, img, img_metas, rescale=False, gt_bboxes=None):
        """Test forward.

         Args:
            img (Tensor): of shape (N, C, H, W) encoding input images.
                Typically these should be mean centered and std scaled.
            img_metas (list[dict]): list of image info dict where each dict
                has: 'img_shape', 'scale_factor', 'flip', and may also contain
                'filename', 'ori_shape', 'pad_shape', and 'img_norm_cfg'.
            rescale (bool): whether to rescale the bboxes.

        Returns:
            dict[str : Tensor]: Track results.
        """
        # TODO inherit from a base tracker
        assert self.with_track_head, 'track head must be implemented.'  # noqa
        frame_id = img_metas[0].get('frame_id', -1)
        if frame_id == 0:
            self.tracker.reset()

        x = self.detector.extract_feat(img)
        proposal_list = self.detector.rpn_head.simple_test_rpn(x, img_metas)#[torch.Size([1000, 5])]

        # det_results = self.detector.roi_head.simple_test(#482长度的list，每个元素是1个array，(x,5)
        #     x, proposal_list, img_metas, rescale=rescale)

        # bbox_results = det_results[0]
        # num_classes = len(bbox_results)
        # outs_det = results2outs(bbox_results=bbox_results) #{labels : array(300,1), bboxes : array(300,5)}

        # det_bboxes = torch.tensor(outs_det['bboxes']).to(img) #放gpu
        # det_labels = torch.tensor(outs_det['labels']).to(img).long() #放gpu

        # gy_add get query
        frame_id = img_metas[0].get('frame_id', -1)
        if frame_id == 0:
            is_random = True
            if is_random:
                gt_bboxes =  gt_bboxes[0].squeeze(0)
                random_bbox_inx = torch.randint(gt_bboxes.shape[0],(1,))
                self.query_bbox = [gt_bboxes[random_bbox_inx]]  
                query_bbox_ori = [gt_bboxes[random_bbox_inx] / torch.tensor(
                                    img_metas[0]['scale_factor']).to(img.device)]
            else:
                raise NotImplementedError()

        #gy_add for saving the gt results
        save_gts = False
        # save_gts = True
        if save_gts:
            the_gt_bboxes = gt_bboxes.cpu().numpy()
            the_gt_bboxes = the_gt_bboxes/img_metas[0]['scale_factor']
            the_gt_bboxes = the_gt_bboxes.astype(int)
            the_gt_color = (0,0,255)
            query_bbox = query_bbox_ori[0].cpu().numpy()
            query_bbox = query_bbox.astype(int)
            query_color = (255,255,0)
            rpn_bboxes = proposal_list[0][:,:4].cpu().numpy()
            rpn_bboxes = rpn_bboxes/img_metas[0]['scale_factor']                
            rpn_bboxes = rpn_bboxes.astype(int)
            color=(255,0,0)
            import cv2
            rpn_img = cv2.imread(img_metas[0]['filename'])

            #draw gt_bboxes
            # topleft = the_gt_bboxes[:, 0:2]
            # downright = the_gt_bboxes[:,2:4]
            # for i in range(the_gt_bboxes.shape[0]):
            #     # print(topleft[i],downright[i])
            #     cv2.rectangle(rpn_img, topleft[i], downright[i], the_gt_color, 2)            


            #draw query_bbox
            # topleft = query_bbox[:, 0:2]
            # downright = query_bbox[:,2:4]
            # cv2.rectangle(rpn_img, topleft[0], downright[0], query_color, 2)

            topleft = rpn_bboxes[:, 0:2]
            downright = rpn_bboxes[:,2:4]
            for i in range(30):
                # print(topleft[i],downright[i])
                cv2.rectangle(rpn_img, topleft[i], downright[i], color, 2)
            save_path = './GMOT40_RPN_test/' + img_metas[0]['filename'].split('/')[-3]
            import os
            if not os.path.exists(save_path):
                os.mkdir(save_path)
            # save_img = save_path + '/' + img_metas[0]['filename'].split('/')[-1]
            # print(save_img)
            # cv2.imwrite(save_img, rpn_img)  
            # cv2.imwrite('gts.jpg', rpn_img)  
            cv2.imwrite('rpn.jpg', rpn_img)  

        det_bboxes = proposal_list[0]
        det_labels = torch.zeros_like(det_bboxes[:,0]).to(img)
        # track_bboxes：torch.Size([214, 5])，track_labels：torch.Size([214])，track_ids：torch.Size([214])
        track_bboxes, track_labels, track_ids = self.tracker.track(
            img_metas=img_metas,
            feats=x,
            model=self,            
            query_bbox=self.query_bbox,
            bboxes=det_bboxes,
            labels=det_labels,
            frame_id=frame_id)

        track_bboxes = outs2results(#长度482的list，每个元素（y,6）
            bboxes=track_bboxes,
            labels=track_labels,
            ids=track_ids,
            num_classes= 1)['bbox_results']

        # return dict(det_bboxes=bbox_results, track_bboxes=track_bboxes)
        return dict(track_bboxes=track_bboxes)
