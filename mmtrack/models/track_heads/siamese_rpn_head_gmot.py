import torch
import torch.nn as nn
from mmcv.cnn.bricks import ConvModule
from mmcv.runner import BaseModule, auto_fp16, force_fp32
from mmdet.core import build_assigner, build_bbox_coder, build_sampler
from mmdet.core.anchor import build_prior_generator
from mmdet.core.bbox.transforms import bbox_xyxy_to_cxcywh
from mmdet.models import HEADS, build_loss

from mmtrack.core.track import depthwise_correlation
from mmtrack.models.track_heads import SiameseRPNHead

@HEADS.register_module()
class SiameseRPNHead_FOR_GMOT(SiameseRPNHead):
    def __init__(self, *args, **kwargs):
        super(SiameseRPNHead_FOR_GMOT, self).__init__(*args, **kwargs)

    # @auto_fp16()
    # def forward(self, z_feats, x_feats):
    #     """Forward with features `z_feats` of exemplar images and features
    #     `x_feats` of search images.

    #     Args:
    #         z_feats (tuple[Tensor]): Tuple of Tensor with shape (N, C, H, W)
    #             denoting the multi level feature maps of exemplar images.
    #             Typically H and W equal to 7.
    #         x_feats (tuple[Tensor]): Tuple of Tensor with shape (N, C, H, W)
    #             denoting the multi level feature maps of search images.
    #             Typically H and W equal to 31.

    #     Returns:
    #         tuple(cls_score, bbox_pred): cls_score is a Tensor with shape
    #         (N, 2 * num_base_anchors, H, W), bbox_pred is a Tensor with shape
    #         (N, 4 * num_base_anchors, H, W), Typically H and W equal to 25.
    #     """
    #     assert isinstance(z_feats, tuple) and isinstance(x_feats, tuple)
    #     assert len(z_feats) == len(x_feats) and len(z_feats) == len(
    #         self.cls_heads)

    #     if self.weighted_sum:
    #         cls_weight = nn.functional.softmax(self.cls_weight, dim=0)
    #         reg_weight = nn.functional.softmax(self.reg_weight, dim=0)
    #     else:
    #         reg_weight = cls_weight = [
    #             1.0 / len(z_feats) for i in range(len(z_feats))
    #         ]

    #     cls_score = 0
    #     bbox_pred = 0
    #     for i in range(len(z_feats)):
    #         cls_score_single = self.cls_heads[i](z_feats[i], x_feats[i])
    #         bbox_pred_single = self.reg_heads[i](z_feats[i], x_feats[i])
    #         cls_score += cls_weight[i] * cls_score_single
    #         bbox_pred += reg_weight[i] * bbox_pred_single

    #     return cls_score, bbox_pred

    @force_fp32(apply_to=('cls_score', 'bbox_pred'))
    def get_bbox(self, cls_score, bbox_pred, prev_bbox, scale_factor):
        """Track `prev_bbox` to current frame based on the output of network.

        Args:
            cls_score (Tensor): of shape (1, 2 * num_base_anchors, H, W).
            bbox_pred (Tensor): of shape (1, 4 * num_base_anchors, H, W).
            prev_bbox (Tensor): of shape (4, ) in [cx, cy, w, h] format.
            scale_factor (Tensr): scale factor.

        Returns:
            tuple(best_score, best_bbox): best_score is a Tensor denoting the
            score of `best_bbox`, best_bbox is a Tensor of shape (4, )
            with [cx, cy, w, h] format, which denotes the best tracked
            bbox in current frame.
        """
        score_maps_size = [(cls_score.shape[2:])]
        if not hasattr(self, 'anchors'):
            self.anchors = self.anchor_generator.grid_priors(
                score_maps_size, device=cls_score.device)[0]
            # Transform the coordinate origin from the top left corner to the
            # center in the scaled feature map.
            feat_h, feat_w = score_maps_size[0]
            stride_w, stride_h = self.anchor_generator.strides[0]
            self.anchors[:, 0:4:2] -= (feat_w // 2) * stride_w
            self.anchors[:, 1:4:2] -= (feat_h // 2) * stride_h

        H, W = score_maps_size[0]
        cls_score = cls_score.view(2, -1, H, W)
        cls_score = cls_score.permute(2, 3, 1, 0).contiguous().view(-1, 2)
        cls_score = cls_score.softmax(dim=1)[:, 1]

        bbox_pred = bbox_pred.view(4, -1, H, W)
        bbox_pred = bbox_pred.permute(2, 3, 1, 0).contiguous().view(-1, 4)
        bbox_pred = self.bbox_coder.decode(self.anchors, bbox_pred)
        bbox_pred = bbox_xyxy_to_cxcywh(bbox_pred)

        filter_idx = cls_score>0.5
        cls_score = cls_score[filter_idx]
        bbox_pred = bbox_pred[filter_idx] / scale_factor 
        from mmdet.core import multiclass_nms
        nms = {'method': 'linear', 'type': 'soft_nms', 'iou_thr': 0.5, 'min_score': 0.001}
        bboxes = bbox_pred
        scores = torch.cat((cls_score.unsqueeze(1), 1-cls_score.unsqueeze(1)),dim=1)
        #bboxes:torch.Size([1000, 4]), scores:torch.Size([1000, 2])
        det_bboxes, det_labels, idx = multiclass_nms(bboxes, scores, 0.05, nms, min(50,bboxes.size(0)), return_inds=True)
        cls_score = cls_score[idx]
        return cls_score, bbox_pred