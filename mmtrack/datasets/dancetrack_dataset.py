# Copyright (c) OpenMMLab. All rights reserved.
from mmdet.datasets import DATASETS

from .mot_challenge_dataset import MOTChallengeDataset
from .gmot_dataset import GMOTDataset
import numpy as np
import motmetrics as mm
from mmcv.utils import print_log
from mmdet.core import eval_map
import os
import os.path as osp
import tempfile

@DATASETS.register_module()
class DanceTrackDataset(MOTChallengeDataset):
    """Dataset for DanceTrack: https://github.com/DanceTrack/DanceTrack.

    Most content is inherited from MOTChallengeDataset.
    """

    def get_benchmark_and_eval_split(self):
        """Get benchmark and dataset split to evaluate.

        Get benchmark from upeper/lower-case image prefix and the dataset
        split to evaluate.

        Returns:
            tuple(string): The first string denotes the type of dataset.
            The second string denots the split of the dataset to eval.
        """
        # As DanceTrack only has train/val and use 'val' for evaluation as
        # default, we can directly output the desired split.
        return 'DanceTrack', 'val'

@DATASETS.register_module()
class DanceTrackDataset_GMOT(GMOTDataset):
    """Dataset for DanceTrack: https://github.com/DanceTrack/DanceTrack.

    Most content is inherited from MOTChallengeDataset.
    """

    def get_benchmark_and_eval_split(self):
        """Get benchmark and dataset split to evaluate.

        Get benchmark from upeper/lower-case image prefix and the dataset
        split to evaluate.

        Returns:
            tuple(string): The first string denotes the type of dataset.
            The second string denots the split of the dataset to eval.
        """
        # As DanceTrack only has train/val and use 'val' for evaluation as
        # default, we can directly output the desired split.
        return 'DanceTrack', 'val'

    def _parse_ann_info(self, img_info, ann_info):
        """Parse bbox and mask annotation.

        Args:
            ann_info (list[dict]): Annotation info of an image.
            with_mask (bool): Whether to parse mask annotations.

        Returns:
            dict: A dict containing the following keys: bboxes, bboxes_ignore,
            labels, masks, seg_map. "masks" are raw annotations and not
            decoded into binary masks.
        """
        gt_bboxes = []
        gt_labels = []
        gt_bboxes_ignore = []
        gt_instance_ids = []
        #gy_add
        # gt_confs = []

        for i, ann in enumerate(ann_info):
            #gy_add
            # ann['visibility'] = 1.0

            # if (not self.test_mode) and (ann['visibility'] <
            #                              self.visibility_thr):
            if (not self.test_mode):
                continue
            x1, y1, w, h = ann['bbox']
            inter_w = max(0, min(x1 + w, img_info['width']) - max(x1, 0))
            inter_h = max(0, min(y1 + h, img_info['height']) - max(y1, 0))
            if inter_w * inter_h == 0:
                continue
            if ann['area'] <= 0 or w < 1 or h < 1:
                continue
            bbox = [x1, y1, x1 + w, y1 + h]
            if ann.get('ignore', False) or ann.get('iscrowd', False):
                # note: normally no `iscrowd` for MOT17Dataset
                gt_bboxes_ignore.append(bbox)
            else:
                gt_bboxes.append(bbox)
                # gy fix
                gt_labels.append(1)
                # gt_confs.append(ann['mot_conf'])
                gt_instance_ids.append(ann['instance_id'])

        if gt_bboxes:
            gt_bboxes = np.array(gt_bboxes, dtype=np.float32)
            gt_labels = np.array(gt_labels, dtype=np.int64)
            #gy_add
            # gt_confs = np.array(gt_confs, dtype=np.float32)
            gt_instance_ids = np.array(gt_instance_ids, dtype=np.int64)
        else:
            gt_bboxes = np.zeros((0, 4), dtype=np.float32)
            gt_labels = np.array([], dtype=np.int64)
            #gy_add
            # gt_confs = np.array([], dtype=np.float32)
            gt_instance_ids = np.array([], dtype=np.int64)

        if gt_bboxes_ignore:
            gt_bboxes_ignore = np.array(gt_bboxes_ignore, dtype=np.float32)
        else:
            gt_bboxes_ignore = np.zeros((0, 4), dtype=np.float32)

        ann = dict(
            bboxes=gt_bboxes,
            labels=gt_labels,
            # confs=gt_confs,
            bboxes_ignore=gt_bboxes_ignore,
            instance_ids=gt_instance_ids)

        return ann

    def evaluate(self,
                 results,
                 metric='track',
                 logger=None,
                 resfile_path=None,
                 bbox_iou_thr=0.5,
                 track_iou_thr=0.5):
        """Evaluation in MOT Challenge.

        Args:
            results (list[list | tuple]): Testing results of the dataset.
            metric (str | list[str]): Metrics to be evaluated. Options are
                'bbox', 'track'. Defaults to 'track'.
            logger (logging.Logger | str | None): Logger used for printing
                related information during evaluation. Default: None.
            resfile_path (str, optional): Path to save the formatted results.
                Defaults to None.
            bbox_iou_thr (float, optional): IoU threshold for detection
                evaluation. Defaults to 0.5.
            track_iou_thr (float, optional): IoU threshold for tracking
                evaluation.. Defaults to 0.5.

        Returns:
            dict[str, float]: MOTChallenge style evaluation metric.
        """
        eval_results = dict()
        if isinstance(metric, list):
            metrics = metric
        elif isinstance(metric, str):
            metrics = [metric]
        else:
            raise TypeError('metric must be a list or a str.')
        allowed_metrics = ['bbox', 'track']
        for metric in metrics:
            if metric not in allowed_metrics:
                raise KeyError(f'metric {metric} is not supported.')

        if 'track' in metrics:
            resfile_path, resfiles, names, tmp_dir = self.format_results(
                results, resfile_path, metrics)
            print_log('Evaluate CLEAR MOT results.', logger=logger)
            distth = 1 - track_iou_thr
            accs = []
            # support loading data from ceph
            local_dir = tempfile.TemporaryDirectory() #gy:'/tmp/tmp4pwrl397'

            for name in names:
                # gy_fix
                gt_file = osp.join(self.img_prefix, f'{name}/gt/gt.txt')
                res_file = osp.join(resfiles['track'], f'{name}.txt')# gy:'/data/GMOT40/track_label/fish-2.txt'
                # copy gt file from ceph to local temporary directory
                gt_dir_path = osp.join(local_dir.name, name, 'gt')# gy:'/tmp/tmp4pwrl397/fish-2/gt'
                os.makedirs(gt_dir_path)
                copied_gt_file = osp.join(
                    local_dir.name,
                    gt_file.replace(gt_file.split(name)[0], '')) # gy:'/tmp/tmp4pwrl397/fish-2.txt'

                f = open(copied_gt_file, 'wb')
                gt_content = self.file_client.get(gt_file)
                if hasattr(gt_content, 'tobytes'):
                    gt_content = gt_content.tobytes()
                f.write(gt_content)
                f.close()
                # copy sequence file from ceph to local temporary directory
                copied_seqinfo_path = osp.join(local_dir.name, name,
                                               'seqinfo.ini') #gy:'/tmp/tmp4pwrl397/fish-2/seqinfo.ini'
                # f = open(copied_seqinfo_path, 'wb')
                # seq_content = self.file_client.get( # gy: 会报错，因为没有seqinfo.ini文件
                #     osp.join(self.img_prefix, name, 'seqinfo.ini'))
                # if hasattr(seq_content, 'tobytes'):
                #     seq_content = seq_content.tobytes()
                # f.write(seq_content)
                # f.close()

                gt = mm.io.loadtxt(copied_gt_file)
                res = mm.io.loadtxt(res_file)
                if osp.exists(copied_seqinfo_path
                              ) and 'MOT15' not in self.img_prefix:
                    acc, ana = mm.utils.CLEAR_MOT_M(
                        gt, res, copied_seqinfo_path, distth=distth)
                else:
                    acc = mm.utils.compare_to_groundtruth(
                        gt, res, distth=distth)
                accs.append(acc)

            mh = mm.metrics.create()
            summary = mh.compute_many(
                accs,
                names=names,
                metrics=mm.metrics.motchallenge_metrics,
                generate_overall=True)

            eval_results.update({
                mm.io.motchallenge_metric_names[k]: v['OVERALL']
                for k, v in summary.to_dict().items()
            })

            # gy_anned
            # eval_results['HOTA'] = hota[-1]
            # summary['HOTA'] = hota

            str_summary = mm.io.render_summary(
                summary,
                formatters=mh.formatters,
                namemap=mm.io.motchallenge_metric_names)
            print(str_summary)
            local_dir.cleanup()
            if tmp_dir is not None:
                tmp_dir.cleanup()

        if 'bbox' in metrics:
            if isinstance(results, dict):
                bbox_results = results['det_bboxes']
            elif isinstance(results, list):
                bbox_results = results
            else:
                raise TypeError('results must be a dict or a list.')
            annotations = [self.get_ann_info(info) for info in self.data_infos]
            #gy_add for ours
            # bbox_results_fix = [bbox['bbox_results'] for bbox in bbox_results]
            mean_ap, _ = eval_map(
                bbox_results,
                # bbox_results_fix,
                annotations,
                iou_thr=bbox_iou_thr,
                dataset=self.CLASSES,
                logger=logger)
            eval_results['mAP'] = mean_ap

        for k, v in eval_results.items():
            if isinstance(v, float):
                eval_results[k] = float(f'{(v):.3f}')

        return eval_results