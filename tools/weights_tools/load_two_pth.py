import torch
reid = torch.load('/home/gaoyun/Desktop/mmtracking/checkpoints/MOT/reid_tao-gmot/epoch_12.pth')
part_train_gmot = torch.load('/home/gaoyun/Desktop/mmtracking/checkpoints/MOT/just_qg_gmot_partv3/epoch_18.pth')
1
total = len(reid['state_dict'])
detectors = len([_ for _ in reid['state_dict'] if 'detector' in _])
for i, k in enumerate(reid['state_dict']):
    if i < detectors:
        print(k)
        reid['state_dict'][k] = part_train_gmot['state_dict'][k[9:]]
torch.save(reid, '/home/gaoyun/Desktop/mmtracking/checkpoints/MOT/gmot_part_with_reid/reid_with_train_partv3_18ep.pth')