for choose_sq in 0 1 2 3 4 5 6
do
    echo $choose_sq
    # for seed in 10 20 30
    for query_idx in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21
    do
    echo $query_idx
    cd /home/gaoyun/Desktop/One-Shot-Object-Detection-master/
    python /home/gaoyun/Desktop/One-Shot-Object-Detection-master/gmot_test_bash.py \
    --s 1 \
    --g 1 \
    --a 4 \
    --cuda \
    --query-idx $query_idx \
    --choose-sq $choose_sq

    cp /home/gaoyun/Desktop/One-Shot-Object-Detection-master/co-ae_detection_label/* /data/GMOT40/co-ae_det_label/

    cd /home/gaoyun/Desktop/mmtracking/
    python /home/gaoyun/Desktop/mmtracking/tools/convert_datasets/mot/gmot_res2coco_one_bash.py \
    -i /data/GMOT40/ \
    -o /data/GMOT40/res_one_annotations/ \
    --choose-sq $choose_sq

    python /home/gaoyun/Desktop/mmtracking/tools/gmot_asso/asso_test_bash.py \
    /home/gaoyun/Desktop/mmtracking/workship/assosiations/co-ae_iou/asso_iou_gmot_one.py \
    --gpu-id 0 \
    --eval track \
    --query-idx $query_idx \
    --choose-sq $choose_sq

    
    done
done