import os
import mmcv
import numpy as np
import seaborn as sns
import random
import cv2
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt

def random_color(seed):
    """Random a color according to the input seed."""
    random.seed(seed)
    # colors = sns.color_palette()
    colors = sns.color_palette("hls", 20)
    color = random.choice(colors)
    return color

def plot_embedding_2D(data, label, title):
    x_min, x_max = np.min(data, 0), np.max(data, 0)
    data = (data - x_min) / (x_max - x_min)
    fig = plt.figure()
    for i in range(data.shape[0]):
        color = random_color(label[i])
        color = (color[0], color[1], color[2], 1.0)
        plt.scatter(data[i, 0], data[i, 1], color = color, marker='o', s=10)
        # plt.text(data[i, 0], data[i, 1], str(label[i]),
        #         #  color=plt.cm.Set1(label[i]),
        #          color=color,
        #          fontdict={'weight': 'bold', 'size': 9})
    plt.xticks([])
    plt.yticks([])
    plt.title(title)
    return fig

# outputs = mmcv.load('/home/gaoyun/Desktop/mmtracking/workship/qg_qdtrack_vis/bboxes_and_features.pkl')
outputs = mmcv.load('/home/gaoyun/Desktop/mmtracking/workship/qg_qdtrack_vis/ball-2.pkl')
# vis_frame = (10, 20)
vis_frame = (0, 100)
# vis_img = '/data/GMOT40/GenericMOT_JPEG_Sequence/car-3/img1/000020.jpg'
vis_img = '/data/GMOT40/GenericMOT_JPEG_Sequence/ball-2/img1/000020.jpg'

# id2bboxes = {}            
# for i, out in enumerate(outputs['track_bboxes']):
#     if i >= vis_frame[0] and i <= vis_frame[1]:
#         track_res = out[0]
#         ids = track_res[:,0]
#         bboxes = track_res[:,1:5]
#         for j in range(track_res.shape[0]):
#             id = int(ids[j])
#             box = bboxes[j:j+1,:]
#             if id not in id2bboxes.keys():
#                 id2bboxes[id] = box
#             else:
#                 id2bboxes[id] = np.concatenate((id2bboxes[id], box), axis=0)
id2embed = {}
del_mode = False
if del_mode:
    del_tracks = [1, 51, 55, 50, 19, 32, 45, 18, 38, 21, 47, 49, 16, 46, 41, 44, 24, 33,53]
    # all_embedings : [(N1, 256),(N2, 256),...,(Nt, 256)]
    all_embedings = outputs['track_features']  
    # id2embed： 保存指定帧范围的id及其所有的feature
    for i, out in enumerate(outputs['track_bboxes']):
        if i >= vis_frame[0] and i <= vis_frame[1]:
            track_res = out[0]
            ids = track_res[:,0]
            embedings = all_embedings[i]
            for j in range(track_res.shape[0]):
                id = int(ids[j])
                embeding = embedings[j:j+1,:]
                # 删除某几个id
                if not del_tracks==None:
                    if id not in del_tracks:
                        if id not in id2embed.keys():
                            id2embed[id] = embeding
                        else:
                            id2embed[id] = np.concatenate((id2embed[id], embeding), axis=0)
else:
    choose_tracks = [2, 3, 4, 8, 10, 12]
    # all_embedings : [(N1, 256),(N2, 256),...,(Nt, 256)]
    all_embedings = outputs['track_features']  
    # id2embed： 保存指定帧范围的id及其所有的feature
    for i, out in enumerate(outputs['track_bboxes']):
        if i >= vis_frame[0] and i <= vis_frame[1]:
            track_res = out[0]
            ids = track_res[:,0]
            embedings = all_embedings[i]
            for j in range(track_res.shape[0]):
                id = int(ids[j])
                embeding = embedings[j:j+1,:]
                # 删除某几个id
                if id in choose_tracks:
                    if id not in id2embed.keys():
                        id2embed[id] = embeding
                    else:
                        id2embed[id] = np.concatenate((id2embed[id], embeding), axis=0)    

# 构造等量的id，用于绘图
id_label = []
re_idx = 0
for k,v in id2embed.items():
    id_label.extend([re_idx]*v.shape[0])
    re_idx = re_idx + 1
id_label = np.array(id_label)

# 拼接所有的embeddings，用于降维
# concat_embeddings：(Nm1*Nm2*...*Nmn, 256)
for i, embeddings in enumerate(id2embed.values()):
    if i == 0:
        concat_embeddings = embeddings
    else: 
        concat_embeddings = np.concatenate((concat_embeddings, embeddings), axis=0)

# TSNE：(Nm1*Nm2*...*Nmn, 256)-->(Nm1*Nm2*...*Nmn, 2)
tsne_2D = TSNE(n_components=2, init='pca', random_state=0)
result_2D = tsne_2D.fit_transform(concat_embeddings)
fig1 = plot_embedding_2D(result_2D, id_label, 't-SNE')
# fig1.show()
fig1.savefig("./embedding.jpg")
