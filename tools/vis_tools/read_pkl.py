import os
import mmcv
import numpy as np
from mmdet.core.evaluation import eval_map

# outputs = mmcv.load('/home/gaoyun/Desktop/mmtracking/best.pkl')
outputs = mmcv.load('/home/gaoyun/Desktop/mmtracking/workship/assosiations/co-ae_deepsort/deepsort_miss4.pkl')
# outputs = mmcv.load('/home/gaoyun/Desktop/mmtracking/workship/assosiations/global_deepsort/deepsort_miss4.pkl')
all_seq = os.listdir('/data/GMOT40/GenericMOT_JPEG_Sequence/')


ignore_seq = ['ball-0','car-1','person-1', 'person-2']
for ig in ignore_seq:
    all_seq.remove(ig)

seq_len = []
for seq_name in all_seq:
    seq_dir = os.path.join('/data/GMOT40/GenericMOT_JPEG_Sequence/', seq_name, 'img1')
    seq_len.append(len(os.listdir(seq_dir)))

cus = 0
cus_len = [0]
for l in seq_len:
    cus += l
    cus_len.append(cus)

split_reslut = 1
1
caculate_seqs = ['fish-2', 'stock-0', 'insect-3', 'balloon-2']
for seq in caculate_seqs:
    idx = all_seq.index(seq)
    idx = cus_len[idx]
    # det_res = [outputs['det_bboxes'][idx]['bbox_results']]
    det_res = [[outputs['track_bboxes'][idx][0][:,1:]]]

    gt_txt = os.path.join('/data/GMOT40/track_label/', seq+'.txt')
    # img = cv2.imread
    with open(gt_txt, 'r') as f:
        gt = []
        for content in f:
            tmp = content.split(',')
            if tmp[0]!='0':
                break
            x = int(tmp[2])
            y = int(tmp[3])
            w = int(tmp[4])
            h = int(tmp[5])
            xyxy = [x, y, x+w, y+h]
            gt.append(xyxy)
    gt = np.array(gt)
    labels = np.zeros(gt.shape[0]).astype(int)
    annotations = [{'bboxes':gt, 'labels':labels}]
    mean_ap, eval_results = eval_map(det_res, annotations)
    1


