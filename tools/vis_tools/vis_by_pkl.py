import os
import mmcv
outputs = mmcv.load('/home/gaoyun/Desktop/mmtracking/workship/qg_qdtrack_vis/ball-2.pkl')

vis = True
if vis:
    import numpy as np
    vis_frame = (10, 20)
    # vis_frame = (10, 20-1)
    # vis_img = '/data/GMOT40/GenericMOT_JPEG_Sequence/car-3/img1/000020.jpg'
    vis_img = '/data/GMOT40/GenericMOT_JPEG_Sequence/ball-2/img1/000020.jpg'
    # vis_img = '/data/GMOT40/GenericMOT_JPEG_Sequence/ball-2/img1/000019.jpg'

    id2bboxes = {}  
    id2scores = {}          
    for i, out in enumerate(outputs['track_bboxes']):
        if i >= vis_frame[0] and i <= vis_frame[1]:
            track_res = out[0]
            ids = track_res[:,0]
            bboxes = track_res[:,1:5]
            scores = track_res[:,5]
            for j in range(track_res.shape[0]):
                id = int(ids[j])
                box = bboxes[j:j+1,:]
                score = scores[j]
                if id not in id2bboxes.keys():
                    id2bboxes[id] = box
                    id2scores[id] = []
                    id2scores[id].append(score)
                else:
                    id2bboxes[id] = np.concatenate((id2bboxes[id], box), axis=0)
                    id2scores[id].append(score)

    import seaborn as sns
    import random
    import cv2
    def custom_color(idx):
        colors = [(66, 1, 158), (78, 64, 214), (71, 117, 245), (154, 232, 254),
               (177, 251, 245), (157, 233, 203), (164, 207, 135), (180, 158, 70),
               (174, 179, 254), (144, 171, 78), (156, 182, 142), (66, 231, 145),
               (90, 4, 68), (32, 230, 248), (0, 0, 0), (169, 0, 99),
               (255, 244, 242)]
        color = colors[idx]
        return color
    def palette_color(idx):
        """Random a color according to the input seed."""
        # random.seed(seed)
        # colors = sns.color_palette()
        colors = sns.color_palette("hls", 17)
        # colors = sns.color_palette("Set1",10)
        # colors = sns.color_palette("tab10",10)
        # colors = sns.color_palette('Accent',8)
        offset_idx = (idx+3)%17
        if offset_idx==9:
            offset_idx = 11
        elif offset_idx==11:
            offset_idx = 9
        color = colors[offset_idx]
        return color
    
    def random_color(seed):
        """Random a color according to the input seed."""
        random.seed(seed)
        # colors = sns.color_palette()
        colors = sns.color_palette("hls", 8)
        # colors = sns.color_palette("Set1",10)
        # colors = sns.color_palette("tab10",10)
        # colors = sns.color_palette('Accent',8)
        color = random.choice(colors)
        return color
    img = cv2.imread(vis_img)
    del_track = [1, 51, 55, 50, 19, 32, 45, 18, 38, 21, 47, 49, 16, 46, 41, 44, 24, 33,53]
    color_idx = 0
    for id, bboxes in id2bboxes.items():
        if bboxes.shape[0] <= 2:
            continue
        if not del_track == None:
            if id in del_track:
                continue 
        for i in range(bboxes.shape[0]):
            if i==0:
                continue
            # color_mode = 'random'
            # color_mode = 'custom'
            # color_mode = 'palette'
            color_mode = 'one'
            if color_mode=='random':         
                tracklet_color = random_color(id)
                tracklet_color = [int(255 * _c) for _c in tracklet_color][::-1]
            elif color_mode=='custom':
                tracklet_color = custom_color(color_idx)
            elif color_mode=='palette':
                tracklet_color = palette_color(color_idx)
                tracklet_color = [int(255 * _c) for _c in tracklet_color][::-1]
            else:
                tracklet_color = (78, 64, 214)
            center_x = bboxes[i-1, 0] + (bboxes[i-1, 2] - bboxes[i-1, 0])/2
            center_y = bboxes[i-1, 1] + (bboxes[i-1, 3] - bboxes[i-1, 1])/2
            pt1 = (int(center_x), int(center_y))
            center_x = bboxes[i, 0] + (bboxes[i, 2] - bboxes[i, 0])/2
            center_y = bboxes[i, 1] + (bboxes[i, 3] - bboxes[i, 1])/2
            pt2 = (int(center_x), int(center_y))
            # cv2.line(img, pt1, pt2, tracklet_color, thickness=4)
            if i==(bboxes.shape[0] - 1):
                text_width, text_height = 9, 13
                font_scale=0.4
                # cv2.circle(img, pt2, 10, tracklet_color, -1)
                x1 = int(bboxes[i, 0])
                y1 = int(bboxes[i, 1])
                x2 = int(bboxes[i, 2])
                y2 = int(bboxes[i, 3])
                cv2.rectangle(img, (x1, y1), (x2, y2), tracklet_color, thickness=3)
                # score
                text = '{:.02f}'.format(id2scores[id][-1])
                # if classes is not None:
                #     text += f'|{classes[label]}'
                width = len(text) * text_width
                # img[y1:y1 + text_height, x1:x1 + width, :] = tracklet_color
                # cv2.putText(
                #     img,
                #     text, (x1, y1 + text_height - 2),
                #     cv2.FONT_HERSHEY_COMPLEX,
                #     font_scale,
                #     color=(0, 0, 0))
                # id
                # text = str(id)
                # width = len(text) * text_width
                # img[y1 + text_height:y1 + 2 * text_height,
                #     x1:x1 + width, :] = tracklet_color
                # cv2.putText(
                #     img,
                #     str(id), (x1, y1 + 2 * text_height - 2),
                #     cv2.FONT_HERSHEY_COMPLEX,
                #     font_scale,
                #     color=(0, 0, 0))
                # cv2.putText(img, str(id), pt1, cv2.FONT_HERSHEY_SIMPLEX, 1.2, tracklet_color, 2)
                cut_patch = True
                if cut_patch:
                    img_patch = img[y1:y2,x1:x2,:]
                    # patch_path = os.path.join('/home/gaoyun/Desktop/mmtracking/GMOT_results/tracklet_vis/track_patch', str(color_idx)+'.jpg')
                    patch_path = os.path.join('/home/gaoyun/Desktop/mmtracking/GMOT_results/tracklet_vis/det_patch', str(color_idx)+'.jpg')
                    cv2.imwrite(patch_path, img_patch)
        color_idx += 1
    cv2.imwrite('/home/gaoyun/Desktop/mmtracking/GMOT_results/tracklet_vis/ball-2_det.jpg', img)
    # cv2.imwrite('/home/gaoyun/Desktop/mmtracking/GMOT_results/tracklet_vis/ball-2_t-1.jpg', img)
