# 1.读取原标签
# 2.随机选择一个开始至少包含一个目标的位置进行裁剪
# 3.标签更改，框、类别
# 4.修改图片另存
# 另还要关心拼接序列的长度、或者色调搭配



from re import X
from unicodedata import category
import mmcv
from mmtrack.datasets.parsers import CocoVID
import cv2
import os
import numpy as np
import random
from tqdm import tqdm
from collections import defaultdict

def croped_gmot_gts(coco_gmot40, cat_vids):
    coco_gmot40



ann_file = '/data/GMOT40/annotations/gmot_cocoformat_miss_4.json'
# ann_file = '/data/MOT17/annotations/train_cocoformat.json'
gmot40 = mmcv.load(ann_file)
coco_gmot40 = CocoVID(ann_file)
outputs = defaultdict(list)
outputs['categories'] = [dict(id=1, name='object')]

# 1.根据各视频序列长度，得到组合序列

reranged_vid = [18,5,14,17,
                2,11,19,6,
                9,1,22,26,
                34,23,27,29,
                20,35,15,30,
                31,25,12,21,
                32,33,8,24,
                28,36,4,13,
                3,7,16,10]
video_len = [len(coco_gmot40.get_img_ids_from_vid(vid)) for vid in reranged_vid]
vid_len_dict = {vid:l for vid,l in zip(reranged_vid, video_len)}

# 2.
width = 1920
height = 1080
save = '/data/exGMOT40/GenericMOT_JPEG_Sequence'
vid_id, img_id, ann_id, ins_id = 1, 1, 1, 0
for i in range(9):
    # 得到四个序列的列表
    cat_vids = reranged_vid[4*i:4*(i+1)]
    vid_id = i
    # 四个序列中的最小图片数量
    min_seq_num = min(vid_len_dict[cat_vids[0]],
                      vid_len_dict[cat_vids[1]],
                      vid_len_dict[cat_vids[2]],
                      vid_len_dict[cat_vids[3]])
    new_w = width // 2
    new_h = height // 2
    offset = {}

    videoname = ''
    for i, cat_vid in enumerate(cat_vids):
        videoname = videoname + coco_gmot40.videos[cat_vid]['name']
    # 按最小图片数量迭代
    txt_path = '/data/exGMOT40/track_label/' + videoname + '.txt'
    with open(txt_path, 'w') as f:
        for j in tqdm(range(min_seq_num)):
            frame_id = j
            new_img = np.zeros([height, width, 3])
            new_seq_name = ''
            new_anns = []
            ins_maps = dict()
            # 迭代其中一个序列
            base_instance_id = 0
            for idx, vid in enumerate(cat_vids):
                imgid = coco_gmot40.get_img_ids_from_vid(vid)[j]
                # get img name
                img_name = coco_gmot40.imgs[imgid]['file_name']
                # get img anns
                img_anns = coco_gmot40.imgToAnns[imgid]
                new_seq_name = new_seq_name + img_name.split('/')[-3]
                if j==0:
                    # 生成平均偏移位置，用于裁剪
                    if len(img_anns) != 0:
                        avg_x = sum([img_ann['bbox'][0] + img_ann['bbox'][2]/2 for img_ann in img_anns]) / len(img_anns)
                        avg_x = int(avg_x)
                        avg_y = sum([img_ann['bbox'][1] + img_ann['bbox'][3]/2 for img_ann in img_anns]) / len(img_anns)
                        avg_y = int(avg_y)
                    offset_x = min(avg_x - new_w // 2, new_w - 1)
                    offset_x = max(0, offset_x)
                    offset_y = min(avg_y - new_h // 2, new_h - 1)
                    offset_y = max(0, offset_y)
                    offset[idx] = (offset_x, offset_y)
                    # 生成随机偏移位置，用于裁剪
                    # anns = coco_gmot40.imgToAnns(imgid)
                    # offset_x = random.choice(range(new_w))
                    # offset_y = random.choice(range(new_h))
                    # offset[idx] = (offset_x, offset_y)

                # read img
                img = cv2.imread(img_name)
                h, w ,c = img.shape
                # resize img and anns
                if not (h==1080 and w==1920):
                    img = cv2.resize(img, (width, height))
                    resize_ritio = (height/h, width/w)
                    for ann in img_anns:
                        ori_x, ori_y, ori_w, ori_h = ann['bbox']
                        ori_x1 = ori_x
                        ori_y1 = ori_y
                        ori_x2 = ori_x + ori_w
                        ori_y2 = ori_y + ori_h
                        ann['bbox'] = [ori_x1 * resize_ritio[1],
                                    ori_y1 * resize_ritio[0],
                                    ori_x2 * resize_ritio[1] - ori_x1 * resize_ritio[1],
                                    ori_y2 * resize_ritio[0] - ori_y1 * resize_ritio[0]]
                h, w ,c = img.shape
                assert h==1080 and w==1920
                o_x, o_y = offset[idx]
                # crop img 
                crop_img = img[o_y : o_y + new_h, o_x : o_x + new_w, :]
                h_idx = idx % 2
                w_idx = idx // 2
                new_img[h_idx * new_h : (h_idx+1) * new_h, 
                        w_idx * new_w : (w_idx+1) * new_w, :] = crop_img
                # crop anns
                for ann in img_anns:
                    x,y,w,h = ann['bbox']
                    x1 = x
                    y1 = y
                    x2 = x + w
                    y2 = y + h
                    if not (x1 >= o_x + new_w or y1 >= o_y + new_h or x2 <= o_x or y2 <= o_y):
                        x1 = max(0, x1 - o_x)
                        x1 = w_idx * new_w + x1
                        y1 = max(0, y1 - o_y)
                        y1 = h_idx * new_h + y1
                        x2 = min(new_w, x2 - o_x)
                        x2 = w_idx * new_w + x2
                        y2 = min(new_h, y2 - o_y)
                        y2 = h_idx * new_h + y2
                        area = (x2 - x1) * (y2 - y1)
                        if area > 250:
                            assert x2 - x1>=0 and y2 - y1>=0
                            assert x2<=1920 and y2<=1080

                            # gy_add
                            mot_instance_id = base_instance_id + ann['mot_instance_id']
                            line = str(int(j)) + ',' + \
                                   str(mot_instance_id) + ',' + \
                                   str(int(x1)) + ',' + \
                                   str(int(y1)) + ',' + \
                                   str(int(x2 - x1)) + ',' + \
                                   str(int(y2 - y1)) + ',1,-1,-1,-1\n'
                            f.write(line)

                            new_ann = dict(
                                category_id = ann['category_id'],
                                bbox = [x1, y1, x2 - x1, y2 - y1],
                                area = area,
                                iscrowd = ann['iscrowd'],
                                mot_conf = ann['mot_conf'],
                                mot_instance_id = mot_instance_id,
                                # image_id = img_id,
                                seq_id = idx,
                            ) 
                            mot_ins_id = new_ann['mot_instance_id']              
                            if mot_ins_id in ins_maps:#gy:ins_maps一开始是个空字典
                                new_ann['instance_id'] = ins_maps[mot_ins_id]
                            else:
                                new_ann['instance_id'] = ins_id#gy:ins_id一开始是0
                            ins_maps[mot_ins_id] = ins_id
                            ins_id += 1 #某一个视频序列中的某一张图片的不同的目标/实例id的个数
                            new_anns.append(new_ann)                    
                base_instance_id = max([k for k,v in ins_maps.items()])
            # 四个序列迭代完成       

            # get new img name
            new_img_name = os.path.join(os.path.join(os.path.join(save, new_seq_name), 'img1'), img_name.split('/')[-1])
            # save img
            if not os.path.exists(os.path.split(new_img_name)[0]):
                os.makedirs(os.path.split(new_img_name)[0])
            cv2.imwrite(new_img_name, new_img)
            
            # 图片信息保存于字典
            image = dict(
                id=img_id,
                video_id=vid_id,
                file_name=new_img_name,
                height=height,
                width=width,
                frame_id=frame_id,
                mot_frame_id=frame_id)
            # new anns信息保存于字典
            for anns in new_anns:
                anns.update(id=ann_id, image_id=img_id)
                outputs['annotations'].append(anns)            
                ann_id += 1
            img_id += 1 #某一个视频序列中的图片id的个数

            outputs['images'].append(image) 

    video = dict(
        id=vid_id,
        name=new_seq_name,
        # fps=fps,
        width=width,
        height=height
    )
    outputs['videos'].append(video)
    vid_id += 1 #视频序列id的个数
# total = sum([v for k,v in video_len.items()])
out_file = '/data/GMOT40/annotations/exgmot_cocoformat_miss_4.json'
mmcv.dump(outputs, out_file)


