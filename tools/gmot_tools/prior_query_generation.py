import os
import mmcv

# ori dect
# coco_format = {
#     'airplane-0':[1,10],
#     'airplane-1':[9,12,17],
#     'airplane-2':[5,8],
#     'airplane-3':[4,8],

#     'ball-0':[1,12],
#     'ball-1':[10,45],
#     'ball-2':[1,8,10,7],
#     'ball-3':[18,14,7],  

#     'balloon-0':[4,50,16,18,21],
#     'balloon-1':[23,30,19],
#     'balloon-2':[1,0,6],
#     'balloon-3':[40,24], 

#     'bird-0':[65, 9],
#     'bird-1':[3],
#     'bird-2':[14,10],
#     'bird-3':[0,4],  

#     'boat-0':[1,36],
#     'boat-1':[18, 2],
#     'boat-2':[2, 16],
#     'boat-3':[12, 4], 

#     'car-0':[28,35],
#     'car-1':[5,10],
#     'car-2':[11, 9],
#     'car-3':[25, 30],    
    
#     'fish-0':[75],
#     'fish-1':[22,14],
#     'fish-2':[6],
#     'fish-3':[1,2,17], 

#     'insect-0':[57,45],
#     'insect-1':[4,5,7],
#     'insect-2':[10,6],
#     'insect-3':[1,7],   

#     'person-0':[12,17],
#     'person-1':[13,5,16],
#     'person-2':[3,8,6],
#     'person-3':[8,11],

#     'stock-0':[3,12,14],
#     'stock-1':[23,12,15],
#     'stock-2':[16],
#     'stock-3':[11,4,7],   
# }

def sq2dict():
    seq_name=[
        'airplane-0','airplane-1','airplane-2','airplane-3',
        'ball-0','ball-1','ball-2','ball-3',    
        'balloon-0','balloon-1','balloon-2','balloon-3',    
        'bird-0','bird-1','bird-2','bird-3',    
        'boat-0','boat-1','boat-2','boat-3',    
        'car-0','car-1','car-2','car-3',    
        'fish-0','fish-1','fish-2','fish-3',    
        'insect-0','insect-1','insect-2','insect-3',    
        'person-0','person-1','person-2','person-3',    
        'stock-0','stock-1','stock-2','stock-3'   
    ]
    good_index=[
        [1,10],[9,12,17],[5,8],[4,8],
        [1,12],[10,45],[1,8,10,7],[18,14,7],
        [4,50,16,18,21],[23,30,19],[1,0,6],[40,24],
        [65, 9], [3], [14,10],[0,4],
        [1,36], [18, 2], [2, 16], [12, 4],
        [28,35], [5,10], [11, 9],[25, 30],
        [75],[22,14],[6],[1,2,17],
        [57,45],[4,5,7],[10,6],[1,7],
        [12,17],[13,5,16],[3,8,6],[8,11],
        [3,12,14],[23,12,15],[16],[11,4,7]
    ]
    coco_format={}
    save_path = './good_index.json'
    for sq, idx in zip(seq_name, good_index):
        coco_format[sq] = idx
    mmcv.dump(coco_format, save_path)   


# dict
# here we keep the good enough results index, 
# and continue to itr other seq
def coco_dict():
    coco_format = {
        'airplane-0':[1,10,0],# 0'MOTA': -0.202 'IDF1': 0.207
        'airplane-1':[9,12,0],#bad
        'airplane-2':[5,8,15],
        'airplane-3':[4,8,2], #0:'MOTA': -0.206 2:'MOTA': -0.101

        'ball-0':[1,12,8],
        'ball-1':[10,45,25],
        # 'ball-2':[1,8,10,7],
        'ball-2':[1,1,1,1],
        'ball-3':[18,14,22],  #12 'MOTA': -0.259,'IDF1': 0.074 17:IDF1: 0.127 'MOTA': -0.222

        # 'balloon-0':[4,50,16,18,21],
        'balloon-0':[4,4,4,4,4],
        # 'balloon-1':[23,30,19],
        'balloon-1':[23,23,23,23],
        # 'balloon-2':[1,0,6],
        'balloon-2':[1,1,1,1],
        # 'balloon-3':[40,24], 
        'balloon-3':[40,40,40,40,40], 

        'bird-0':[65, 9, 59],
        'bird-1':[3,11,5],# 5
        'bird-2':[14,10, 15],
        'bird-3':[0,4, 5],  #3: 'MOTA': -0.278 4:'MOTA': -0.241 idf:4.9% 5 'MOTA': -0.168 idf:6.5%

        # 'boat-0':[1,36],
        'boat-0':[36,36 ,36],
        'boat-1':[18, 2, 14],
        # 'boat-2':[2, 16],
        'boat-2':[2, 2, 2, 2],
        # 'boat-3':[12, 4], 
        'boat-3':[4, 4, 4], 

        'car-0':[28,35,14],
        'car-1':[5,10,8],
        # 'car-2':[11, 9],
        'car-2':[9, 9, 9],
        # 'car-3':[25, 30],    
        'car-3':[25, 25, 25, 25],    
        
        # 'fish-0':[75],
        'fish-0':[75,75,75,75,75],
        # 'fish-1':[22,14],
        'fish-1':[22,22,22,22],
        # 'fish-2':[6, 36],
        'fish-2':[36, 36,36],
        'fish-3':[1,2,17], 

        'insect-0':[57,45,3],#0: MOTA -0.7% 2: 3: MOTA:4.3% IDF1:11.4% 先测到9
        'insect-1':[4,5,1], #1:'MOTA': -0.206 IDF1:4.1%
        'insect-2':[10,6,3], #3:'MOTA': -0.008 good
        'insect-3':[1,3,6],  #bad 6最好

        # 'person-0':[12,17],
        'person-0':[17,17,17],
        'person-1':[13,5,16],
        'person-2':[3,8,6],
        'person-3':[8,11,7],

        # 'stock-0':[3,12,14],
        'stock-0':[3,3,3],
        # 'stock-1':[23,12,15],
        'stock-1':[12,12,12],
        'stock-2':[16,11,20],
        'stock-3':[11,4,6],   #6: 'MOTA': -0.36
    }
    save_path = '/home/gaoyun/Desktop/mmtracking/tools/gmot_tools/bad_index_v3.json'
    mmcv.dump(coco_format, save_path)
    print(f'{save_path} Done!')

coco_dict()

