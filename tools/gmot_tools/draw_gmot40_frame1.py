import mmcv
from mmtrack.datasets.parsers import CocoVID
import cv2
import os
# ann_file = '/data/GMOT40/annotations/gmot_cocoformat_miss_4.json'
ann_file = '/data/GMOT40/annotations/exgmot_cocoformat_miss_4.json'
save_dir = '/home/gaoyun/Desktop/mmtracking/GMOT_results/exGMOT40_frame1_vis/'
gmot40 = CocoVID(ann_file)
# gmot40 = mmcv.load(ann_file)
gmot40

for i in range(len(gmot40.imgs)):
    img_id = gmot40.imgs[i+1]['id']
    full_file_name = gmot40.imgs[i+1]['file_name']
    frame_id = gmot40.imgs[i+1]['frame_id']
    if frame_id==0:
        img = cv2.imread(full_file_name)
        anns = gmot40.imgToAnns[img_id]
        for j, ann in enumerate(anns):
            x,y,w,h = ann['bbox']
            x1 = int(x)
            y1 = int(y)
            x2 = int(x + w)
            y2 = int(y + h)
            cv2.rectangle(img, [x1, y1], [x2, y2], (0, 255, 0), 2)
            cv2.putText(img, str(j), (x2, y2), cv2.FONT_HERSHEY_SIMPLEX, 0.75, color=(0, 0, 255), thickness=1)
        file_name = full_file_name.split('/')[-1]
        class_name = full_file_name.split('/')[-3]
        save_path = os.path.join(os.path.join(save_dir, class_name), file_name)
        if not os.path.exists(os.path.join(save_dir, class_name)):
            os.mkdir(os.path.join(save_dir, class_name))
        cv2.imwrite(save_path, img)

