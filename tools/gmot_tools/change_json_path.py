import mmcv
from mmtrack.datasets.parsers import CocoVID
import cv2
import os.path as osp
ann_file = '/data/GMOT40/annotations/gmot_cocoformat_airplane-1.json'
changed_image_path = '/home/gaoyun/Desktop/mmtracking/GMOT_results/GMOT40_one_results/GMOT40/GenericMOT_JPEG_Sequence/airplane-1/img1'
save_dir = '/home/gaoyun/Desktop/mmtracking/tools/gmot_tools/changed_img_path_airplane-1.json'
# gmot40 = CocoVID(ann_file)
gmot40 = mmcv.load(ann_file)
gmot40

dataset = gmot40['images']
for i in range(len(dataset)):
    dataset[i]['file_name'] = osp.join(changed_image_path,dataset[i]['file_name'].split('/')[-1])
gmot40['dataset'] = dataset
mmcv.dump(gmot40, save_dir)