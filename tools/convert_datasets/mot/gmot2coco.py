# Copyright (c) OpenMMLab. All rights reserved.
# This script converts MOT labels into COCO style.
# Official website of the MOT dataset: https://motchallenge.net/
#
# Label format of MOT dataset:
#   GTs:
#       <frame_id> # starts from 1 but COCO style starts from 0,
#       <instance_id>, <x1>, <y1>, <w>, <h>,
#       <conf> # conf is annotated as 0 if the object is ignored,
#       <class_id>, <visibility>
#
#   DETs and Results:
#       <frame_id>, <instance_id>, <x1>, <y1>, <w>, <h>, <conf>,
#       <x>, <y>, <z> # for 3D objects
#
# Classes in MOT:
#   1: 'pedestrian'
#   2: 'person on vehicle'
#   3: 'car'
#   4: 'bicycle'
#   5: 'motorbike'
#   6: 'non motorized vehicle'
#   7: 'static person'
#   8: 'distractor'
#   9: 'occluder'
#   10: 'occluder on the ground',
#   11: 'occluder full'
#   12: 'reflection'
#
#   USELESS classes are not included into the json file.
#   IGNORES classes are included with `ignore=True`.
import argparse
import os
import os.path as osp
from collections import defaultdict

import mmcv
import numpy as np
from tqdm import tqdm


def parse_args():
    parser = argparse.ArgumentParser(
        description='Convert MOT label and detections to COCO-VID format.')
    parser.add_argument('-i', '--input', help='path of MOT data')
    parser.add_argument(
        '-o', '--output', help='path to save coco formatted label file')
    return parser.parse_args()



def parse_gmot_gts(gts):
    outputs = defaultdict(list)
    for gt in gts:
        gt = gt.strip().split(',')
        frame_id, ins_id = map(int, gt[:2])
        bbox = list(map(float, gt[2:6]))
        conf = float(gt[6])
        # [x1, y1, w, h] to be consistent with mmdet
        bbox = [
            # bbox[0], bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3]
            bbox[0], bbox[1], bbox[2], bbox[3]
        ]
        anns = dict(
            category_id=1,
            bbox=bbox,
            area=bbox[2] * bbox[3],
            iscrowd=False,
            mot_conf=conf,
            mot_instance_id=ins_id)
        outputs[frame_id].append(anns)

    return outputs

def main():
    args = parse_args()
    if not osp.isdir(args.output):
        os.makedirs(args.output)


    vid_id, img_id, ann_id = 1, 1, 1

    
    subset = 'gmot'
    ins_id = 0
    print(f'Converting {subset} set to COCO format')


    sequence_folder = osp.join(args.input, 'GenericMOT_JPEG_Sequence')
    gt_folder = osp.join(args.input, 'track_label')
    out_file = osp.join(args.output, f'{subset}_cocoformat.json')#gy:输出文件
    outputs = defaultdict(list)
    outputs['categories'] = [dict(id=1, name='object')]

    video_names = os.listdir(sequence_folder)
    for video_name in tqdm(video_names):
        # basic params
        ins_maps = dict()
        # load video infos
        video_folder = osp.join(sequence_folder, video_name)
        img_path = osp.join(video_folder, 'img1')
        img_names = os.listdir(img_path)
        img_names = sorted(img_names) #gy：所有图片的名字
        # fps = int(infos[3].strip().split('=')[1]) #gy：帧率
        num_imgs = len(img_names) #gy：图片数量/序列长度
        assert num_imgs > 0
        img = mmcv.imread(img_path + '/' + img_names[0])
        height, width = img.shape[:2]
        video = dict(
            id=vid_id,
            name=video_name,
            # fps=fps,
            width=width,
            height=height
        )
        gts = mmcv.list_from_file(f'{gt_folder}/{video_name}.txt')
        img2gts = parse_gmot_gts(gts)

        # image and box level infos
        for frame_id, name in enumerate(img_names):
            img_name = osp.join(img_path, name)
            mot_frame_id = int(name.split('.')[0])
            image = dict(
                id=img_id,
                video_id=vid_id,
                file_name=img_name,
                height=height,
                width=width,
                frame_id=frame_id,
                mot_frame_id=mot_frame_id)

            gts = img2gts[mot_frame_id]
            for gt in gts:#gy：没看明白这个循环的逻辑，好像是一个映射关系，将不规律的目标id映射为顺序的
                gt.update(id=ann_id, image_id=img_id)#添加id和image_id
                mot_ins_id = gt['mot_instance_id']#某个目标的id  
                if mot_ins_id in ins_maps:#gy:ins_maps一开始是个空字典
                    gt['instance_id'] = ins_maps[mot_ins_id]
                else:
                    gt['instance_id'] = ins_id#gy:ins_id一开始是0
                    ins_maps[mot_ins_id] = ins_id
                    ins_id += 1 #某一个视频序列中的某一张图片的不同的目标/实例id的个数
                outputs['annotations'].append(gt)
                ann_id += 1 #某一个视频序列中的某一张图片的目标/实例id的总个数
            
            outputs['images'].append(image) 
            img_id += 1 #某一个视频序列中的图片id的个数
        outputs['videos'].append(video)
        vid_id += 1 #视频序列id的个数
        outputs['num_instances'] = ins_id
    print(f'{subset} has {ins_id} instances.')
    mmcv.dump(outputs, out_file)    
    print(f'Done! Saved as {out_file}')


if __name__ == '__main__':
    main()
