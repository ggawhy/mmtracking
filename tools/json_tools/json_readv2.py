import json
import mmcv
import os
import pandas as pd
import numpy as np

# jsons_path = '/home/gaoyun/Desktop/mmtracking/workship/qg_qdtrack_query_update/qg_qd_update'
# jsons_path = '/home/gaoyun/Desktop/mmtracking/workship/qg_qdtrack_query_update/qg_qd_update50'
# jsons_path = '/home/gaoyun/Desktop/mmtracking/workship/mask_feature/mask_exp2'
# jsons_path = '/home/gaoyun/Desktop/mmtracking/workship/mask_feature/mask_seed_exp_no'
# jsons_path = '/home/gaoyun/Desktop/mmtracking/workship/mask_corr_feature/exp_no'
jsons_path = '/home/gaoyun/Desktop/mmtracking/workship/mask_query/mask_exp_no'
save_key = ['MOTA', 'IDF1']

jsons = sorted(os.listdir(jsons_path))
index = []
data = []
for json_name in jsons:
    
    json_full_name = os.path.join(jsons_path, json_name)
    # index.append(json_name[:2]+'_'+json_name[7:9])
    index.append(json_name[:-9])
    jsonfile = mmcv.load(json_full_name)
    value = []
    for k in save_key:
        value.append(jsonfile[k])
    value.append(float(json_name[-11:-9])/100)
    data.append(value)

save_key.append('mask_ratio')
data = np.array(data)
df = pd.DataFrame(data=data,index=index,columns=save_key)
# df.to_csv('query_update.csv')
# df.to_csv('query_mask_feature.csv')
# df.to_csv('query_mask_corr.csv')
df.to_csv('query_mask_img.csv')
