import json
import mmcv
import os
import pandas as pd
import numpy as np

jsons_path = '/home/gaoyun/Desktop/mmtracking/workship/qg_qd_queryset_combine/queryset_combine_noprior_exp/mean'
save_key = ['MOTA', 'IDF1']

jsons = sorted(os.listdir(jsons_path))
index = []
data = []
for json_name in jsons:
    
    json_full_name = os.path.join(jsons_path, json_name)
    # index.append(json_name[:2]+'_'+json_name[7:9])
    index.append(json_name[:-9])
    jsonfile = mmcv.load(json_full_name)
    value = []
    for k in save_key:
        value.append(jsonfile[k])
    data.append(value)

data = np.array(data)
df = pd.DataFrame(data=data,index=index,columns=save_key)
# df.to_csv('query_update.csv')
# df.to_csv('query_mask_feature.csv')
# df.to_csv('query_mask_corr.csv')
df.to_csv('mean.csv')
