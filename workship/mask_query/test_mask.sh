# for seed in 10 30 40 50 60 70 80 90 
for mask_ratio in 0.0 0.1 0.2 0.3 0.4 0.5
do
echo $mask_ratio
for seed in 10 20 30 40 50 60 70 80 90 
do
echo $seed
python /home/gaoyun/Desktop/mmtracking/workship/mask_query/test_mask.py \
/home/gaoyun/Desktop/mmtracking/workship/mask_query/qd_qg_mask_query_no.py \
--checkpoint /home/gaoyun/Desktop/mmtracking/checkpoints/MOT/reid_tao-gmot/epoch_12.pth \
--work-dir /home/gaoyun/Desktop/mmtracking/workship/mask_query/mask_exp_no \
--gpu-id 0 \
--eval track \
--mask-ratio $mask_ratio \
--seed $seed
done
done