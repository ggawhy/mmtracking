# for seed in 10 30 40 50 60 70 80 90 
for seed in 15 25 35 45 55 65 75 85 95 
do
echo $seed
python /home/gaoyun/Desktop/mmtracking/workship/qg_qd_seed/test_seed.py \
/home/gaoyun/Desktop/mmtracking/workship/qg_qd_seed/qdtrack_qg_gmot40_softnms_miss_4_no_prior.py \
--checkpoint /home/gaoyun/Desktop/mmtracking/checkpoints/MOT/reid_tao-gmot/epoch_12.pth \
--work-dir /home/gaoyun/Desktop/mmtracking/workship/qg_qd_seed \
--gpu-id 0 \
--eval track \
--seed $seed
done