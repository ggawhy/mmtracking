import os
import random

gt1_dir = '/data/GMOT40/co-ae_det_label/'
gt2_dir = '/data/GMOT40/globaltrack_det_label/'
gt3_dir = '/data/GMOT40/os2d_det_label/'
traget_path = '/data/GMOT40/combinev1_det_label/'

os2d_ignore = ['boat-3','ball-3','ball-1','bird-0','stock-1','airplane-3','airplane-2','car-0','person-3','insect-1','boat-2','stock-3']
gts = os.listdir(gt1_dir)
with open('/home/gaoyun/Desktop/mmtracking/workship/assosiations/combinev1_deepsort/combine.txt','a') as f:
    for i, gt in enumerate(gts):
        # random.seed(5)
        if gt[:-4] in os2d_ignore:
            tmp = random.randint(0, 1)
        else:
            randseed = random.randint(0, 100)
            if randseed<10:
                tmp = 2
            elif randseed>=10 and randseed<70:
                tmp = 1
            else:
                tmp = 0
            # tmp = random.randint(0, 2)
        if i==39:
            f.write(str(tmp)+'\n')
        else:
            f.write(str(tmp)+' ')
        if tmp==0:
            gt_txt = os.path.join(gt1_dir, gt)
            cmd = 'cp' + ' ' + gt_txt + ' ' + traget_path
            print(cmd)
            os.system(cmd)
        elif tmp==1:
            gt_txt = os.path.join(gt2_dir, gt)
            cmd = 'cp' + ' ' + gt_txt + ' ' + traget_path
            print(cmd)
            os.system(cmd)
        elif tmp==2:
            gt_txt = os.path.join(gt3_dir, gt)
            cmd = 'cp' + ' ' + gt_txt + ' ' + traget_path
            print(cmd)
            os.system(cmd)